package com.weback

import android.app.Application
import io.apptegrity.Apptegrity

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Apptegrity.init(this.applicationContext, "9AH37SX-JF54BKW-GCECJCC-ZQ5MWXH")
    }
}