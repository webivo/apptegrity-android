package io.apptegrity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.telephony.TelephonyManager
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import io.apptegrity.business.di.AppContainer
import io.apptegrity.ui.notifications.NotificationsActivity
import java.lang.Exception
import java.util.Locale

object Apptegrity {

    private lateinit var _appContainer: AppContainer

    val appContainer: AppContainer?
        get() = _appContainer

    fun init(context: Context?, apiKey: String) {
        _appContainer = AppContainer()
        _appContainer.setAppContainer(apiKey)
        getLocation(context)
        val sharedPreferences = context?.getSharedPreferences("io.apptegrity", Context.MODE_PRIVATE)
        val telephonyManager =
            context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val currentLocale = context.resources.configuration.locale
        _appContainer.sharedPreferences = sharedPreferences
        _appContainer.setCountryCodeValue(telephonyManager.networkCountryIso)
        _appContainer.setCountryCodeLocale(currentLocale.country)
        _appContainer.createUID()
    }

    fun start(context: Context) {
        context.startActivity(Intent(context, NotificationsActivity::class.java))
    }

    /*
    Check for potential memory leaks
     */
    private fun getLocation(context: Context?) {
        context?.let { it ->
            if (ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
            ) {
                val geoCoder = Geocoder(it, Locale.getDefault())
                val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
                fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                    try {
                        val addresses =
                            geoCoder.getFromLocation(location.latitude, location.longitude, 1)
                        _appContainer.setCity(addresses[0].locality)
                        _appContainer.setCountry(addresses[0].countryName)
                        _appContainer.setCountryCode(addresses[0].countryCode)
                    } catch (exception: Exception) {
                        exception.printStackTrace()
                    }
                }
            }
        }
    }
}
