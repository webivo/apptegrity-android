package io.apptegrity.business.api

import io.apptegrity.business.model.Notification
import io.apptegrity.business.model.Feedback
import io.apptegrity.business.utils.GET_ALL_FEATURES
import io.apptegrity.business.utils.POST_ALL_FEATURES_PAGE_VIEW
import io.apptegrity.business.utils.POST_FEATURE_FEEDBACK
import io.apptegrity.business.utils.PUT_FEATURE_OPENED
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface NotificationsApi {

    @GET(GET_ALL_FEATURES)
    fun getAllFeatures(): Single<List<Notification>>

    @Headers("Content-Type: application/json")
    @POST(POST_FEATURE_FEEDBACK)
    fun sendFeatureFeedback(@Path("featureId") featureId: String, @Body feedback: Feedback): Completable

    @Headers("Content-Type: application/json")
    @PUT(PUT_FEATURE_OPENED)
    fun sendOnFeatureOpened(@Path("featureId") featureId: String): Completable

    @Headers("Content-Type: application/json")
    @POST(POST_ALL_FEATURES_PAGE_VIEW)
    fun sendOnAllFeaturesOpened(): Completable
}
