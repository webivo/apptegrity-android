package io.apptegrity.business.di

import android.content.SharedPreferences
import io.apptegrity.business.api.NotificationsApi
import io.apptegrity.business.usecase.NotificationUseCaseImpl
import io.apptegrity.business.utils.BASE_URL_DEV
import io.apptegrity.business.utils.BASE_URL_PROD
import io.apptegrity.business.utils.HEADER_AUTHORIZATION_KEY
import io.apptegrity.business.utils.HEADER_VISITOR_ID_KEY
import io.apptegrity.business.utils.VISITOR_ID_KEY
import io.apptegrity.business.utils.HEADER_USER_AGENT_KEY
import io.apptegrity.business.utils.X_COUNTRY_CODE_KEY
import io.apptegrity.business.utils.X_COUNTRY_KEY
import io.apptegrity.business.utils.X_CITY_KEY
import io.apptegrity.ui.notificationdetails.NotificationDetailsViewModel
import io.apptegrity.ui.notifications.NotificationsViewModel
import okhttp3.CertificatePinner
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.UUID
import java.util.concurrent.TimeUnit

class AppContainer {

    private var apiKey: String? = null
    private var userId: String? = null
    private var countryCodeValue: String? = null
    private var localeCountryCode: String? = null
    private var userAgent = "unknown"
    private var city: String? = null
    private var country: String? = null
    private var countryCode: String? = null

    private val certificatePinner = CertificatePinner.Builder()
        .add("https://api.apptegrity.io", "sha256/CeueSgN16aulNkZ4nNt3wIIg5dhafnBk8aK5V+By1kY=")
        .build()

    private val client = OkHttpClient.Builder()
        .pingInterval(10, TimeUnit.SECONDS)
        .connectTimeout(20, TimeUnit.SECONDS)
        .readTimeout(20, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .certificatePinner(certificatePinner)
        .addInterceptor(createLoggingInterceptor())
        .addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val originalRequest = chain.request()

                userId = getUID()

                val request = originalRequest.newBuilder()
                    .method(originalRequest.method, originalRequest.body)
                    .header(HEADER_AUTHORIZATION_KEY, "Bearer $apiKey")
                    .header(HEADER_VISITOR_ID_KEY, "$userId")
                    .header(
                        HEADER_USER_AGENT_KEY,
                        System.getProperty("http.agent") ?: userAgent
                    )

                if (country != null && city != null && countryCode != null) {
                    request.header(X_COUNTRY_CODE_KEY, countryCode!!)
                        .header(X_COUNTRY_KEY, country!!)
                        .header(X_CITY_KEY, city!!)
                } else if (countryCodeValue != null) {
                    request.header(X_COUNTRY_CODE_KEY, countryCodeValue!!)
                } else {
                    request.header(X_COUNTRY_CODE_KEY, localeCountryCode!!)
                }

                return chain.proceed(request.build())
            }
        }).build()


    val notificationsApi: NotificationsApi = Retrofit.Builder()
        .baseUrl(BASE_URL_DEV)
        .client(client)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(NotificationsApi::class.java)

    var sharedPreferences: SharedPreferences? = null

    var featureContainer: FeatureContainer? = null
    var featureDetailsContainer: FeatureDetailsContainer? = null

    fun setAppContainer(apiKey: String) {
        this.apiKey = apiKey
    }

    fun setCountryCodeValue(value: String) {
        countryCodeValue = value
    }

    fun setCountryCodeLocale(value: String) {
        localeCountryCode = value
    }

    fun setCity(value: String) {
        city = value
    }

    fun setCountry(value: String) {
        country = value
    }

    fun setCountryCode(value: String) {
        countryCode = value
    }

    fun createUID() {
        val visitorId = sharedPreferences?.getString(VISITOR_ID_KEY, null)
        if (visitorId.isNullOrEmpty()) {
            val uid = UUID.randomUUID().toString()
            sharedPreferences?.edit()?.putString(VISITOR_ID_KEY, uid)?.apply()
        }
    }

    fun getUID(): String? {
        return sharedPreferences?.getString(VISITOR_ID_KEY, null)
    }

    private fun createLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.HEADERS
        return interceptor
    }
}

class FeatureContainer(notificationsApi: NotificationsApi) {
    private val featureUseCase = NotificationUseCaseImpl(notificationsApi)
    val featureViewModel = NotificationsViewModel(featureUseCase)
}

class FeatureDetailsContainer(notificationsApi: NotificationsApi) {
    private val featureUseCase = NotificationUseCaseImpl(notificationsApi)
    val featureDetailsViewModel = NotificationDetailsViewModel(featureUseCase)
}
