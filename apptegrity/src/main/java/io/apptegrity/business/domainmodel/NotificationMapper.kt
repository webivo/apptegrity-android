package io.apptegrity.business.domainmodel

import io.apptegrity.business.model.Notification
import io.apptegrity.business.model.Feedback
import io.apptegrity.business.utils.epochToDate
import io.apptegrity.ui.notifications.model.NotificationModel
import io.apptegrity.ui.notifications.model.FeedbackModel

class NotificationMapper {

    companion object {
        fun fromFeature(allNotifications: List<Notification>): List<NotificationModel> {
            val allFeaturesModel = mutableListOf<NotificationModel>()
            allNotifications
                .sortedByDescending {
                    it.timestamp._seconds
                }.map { feature ->
                    allFeaturesModel.add(
                        NotificationModel(
                            feature.id,
                            feature.content,
                            feature.title,
                            feature.imageUrl,
                            feature.state.value,
                            feature.timestamp._seconds.epochToDate()
                        )
                    )
                }

            return allFeaturesModel
        }

        fun toFeedBack(feedback: FeedbackModel): Feedback {
            return Feedback(feedback.text)
        }
    }
}
