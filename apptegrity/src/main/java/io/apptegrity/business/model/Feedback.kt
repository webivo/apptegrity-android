package io.apptegrity.business.model

data class Feedback(
    val text: String
)