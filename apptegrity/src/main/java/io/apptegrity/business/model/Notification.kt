package io.apptegrity.business.model

data class Notification(
    val id: String,
    val content: String,
    val title: String,
    val imageUrl: String?,
    val timestamp: Timestamp,
    val state: State
)