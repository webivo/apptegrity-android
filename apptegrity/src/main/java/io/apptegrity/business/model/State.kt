package io.apptegrity.business.model

data class State(
    val value: String,
    val label: String
)