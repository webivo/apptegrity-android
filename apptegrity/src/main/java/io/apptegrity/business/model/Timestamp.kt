package io.apptegrity.business.model

data class Timestamp(
    val _seconds: Long,
    val _nanoseconds: Long
)