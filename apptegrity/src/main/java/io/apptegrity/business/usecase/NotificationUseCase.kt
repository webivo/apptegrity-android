package io.apptegrity.business.usecase

import io.apptegrity.business.model.Feedback
import io.apptegrity.ui.notifications.model.NotificationModel
import io.reactivex.Completable
import io.reactivex.Single

interface NotificationUseCase {

    fun getAllFeatures(): Single<List<NotificationModel>>

    fun sendFeedback(featureId: String, feedback: Feedback): Completable

    fun sendOnFeatureOpened(featureId: String): Completable

    fun sendOnAllFeaturesOpened(): Completable
}
