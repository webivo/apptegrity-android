package io.apptegrity.business.usecase

import io.apptegrity.business.api.NotificationsApi
import io.apptegrity.business.domainmodel.NotificationMapper
import io.apptegrity.business.model.Feedback
import io.apptegrity.ui.notifications.model.NotificationModel
import io.reactivex.Completable
import io.reactivex.Single

class NotificationUseCaseImpl(private val notificationsApi: NotificationsApi) :
    NotificationUseCase {

    override fun getAllFeatures(): Single<List<NotificationModel>> {
        return notificationsApi.getAllFeatures()
            .map { allFeatures ->
                NotificationMapper.fromFeature(allFeatures)
            }
    }

    override fun sendFeedback(featureId: String, feedback: Feedback): Completable {
        return notificationsApi.sendFeatureFeedback(featureId, feedback)
    }

    override fun sendOnFeatureOpened(featureId: String): Completable {
        return notificationsApi.sendOnFeatureOpened(featureId)
    }

    override fun sendOnAllFeaturesOpened(): Completable {
        return notificationsApi.sendOnAllFeaturesOpened()
    }
}
