package io.apptegrity.business.utils

const val BASE_URL_DEV = "https://api-dev-apptegrity.firebaseapp.com/"
const val BASE_URL_PROD = "https://api.apptegrity.io/"
const val HEADER_AUTHORIZATION_KEY = "Authorization"
const val HEADER_VISITOR_ID_KEY = "VisitorID"
const val HEADER_USER_AGENT_KEY = "User-Agent"
const val VISITOR_ID_KEY = "VISITOR ID"
const val X_COUNTRY_CODE_KEY = "X-Country-Code"
const val X_COUNTRY_KEY = "X-Country"
const val X_CITY_KEY = "X-City"