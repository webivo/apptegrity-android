package io.apptegrity.business.utils

const val GET_ALL_FEATURES = "/features"
const val POST_FEATURE_FEEDBACK = "/features/{featureId}/feedback"
const val PUT_FEATURE_OPENED = "/features/{featureId}/opened"
const val POST_ALL_FEATURES_PAGE_VIEW = "/whatsnew/seen"
