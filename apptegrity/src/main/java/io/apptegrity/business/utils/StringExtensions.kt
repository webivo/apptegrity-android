package io.apptegrity.business.utils

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit

fun Long.epochToDate(): String {
    val millis = TimeUnit.SECONDS.toMillis(this)
    val date = Date(millis)
    val format = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
    return format.format(date)
}