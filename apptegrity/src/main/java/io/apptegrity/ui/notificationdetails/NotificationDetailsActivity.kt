package io.apptegrity.ui.notificationdetails

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import io.apptegrity.Apptegrity
import io.apptegrity.R
import io.apptegrity.business.di.AppContainer
import io.apptegrity.business.di.FeatureDetailsContainer
import io.apptegrity.ui.notifications.model.NotificationModel
import io.apptegrity.ui.utils.Event
import io.apptegrity.ui.utils.FEATURE_DETAILS
import kotlinx.android.synthetic.main.activity_feature_details.titleTextView
import kotlinx.android.synthetic.main.activity_feature_details.contentView
import kotlinx.android.synthetic.main.activity_feature_details.progressBar
import kotlinx.android.synthetic.main.activity_feature_details.sendImageView
import kotlinx.android.synthetic.main.activity_feature_details.feedbackEditText
import kotlinx.android.synthetic.main.activity_feature_details.detailsImageView
import kotlinx.android.synthetic.main.toolbar_layout.backImageView
import kotlinx.android.synthetic.main.toolbar_layout.dateTextView

class NotificationDetailsActivity : AppCompatActivity() {

    private var appContainer: AppContainer? = null
    private lateinit var notificationDetailsViewModel: NotificationDetailsViewModel

    private val onFeatureDetailsObserver = Observer<NotificationModel> { feature ->
        titleTextView.text = feature.title
        setFeatureImage(feature.imageUrl)
        contentView.text = HtmlCompat.fromHtml(feature.content, HtmlCompat.FROM_HTML_MODE_LEGACY)
        dateTextView.text = feature.date
        contentView.setOnTouchListener { _, event -> event.action == MotionEvent.ACTION_MOVE }
    }

    private val onFeatureSendFeedBackObserver = Observer<Event> { event ->
        progressBar.visibility = View.GONE
        sendImageView.visibility = View.VISIBLE
        feedbackEditText.text.clear()
        feedbackEditText.isEnabled = true
        when (event) {
            is Event.NetworkSuccess -> showMessage(getString(R.string.feature_details_feedback_success_text))
            is Event.ObjectNull -> showMessage(getString(R.string.feature_details_empty_feedback_error_text))
            is Event.NetworkError -> showMessage(getString(R.string.feature_details_network_error_text))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_feature_details)

        val notification: NotificationModel? = intent.extras?.getParcelable(FEATURE_DETAILS)

        appContainer = Apptegrity.appContainer
        appContainer?.let {
            it.featureDetailsContainer = FeatureDetailsContainer(it.notificationsApi)
            val featureDetailsContainer = it.featureDetailsContainer
            if (featureDetailsContainer != null) {
                notificationDetailsViewModel = featureDetailsContainer.featureDetailsViewModel
                registerObservers()
                notificationDetailsViewModel.showFeatureDetails(notification)
                notificationDetailsViewModel.sendOnFeatureOpened(notification?.id)
                sendFeedback(notification?.id)
            }
        }
        goBack()
    }

    override fun onDestroy() {
        appContainer?.featureDetailsContainer = null
        super.onDestroy()
    }

    private fun registerObservers() {
        notificationDetailsViewModel.onNotificationDetailsEvent.observe(
            this,
            onFeatureDetailsObserver
        )
        notificationDetailsViewModel.onSendFeedBackEvent.observe(
            this,
            onFeatureSendFeedBackObserver
        )
    }

    private fun setFeatureImage(imageUrl: String?) {
        detailsImageView.visibility = View.VISIBLE
        Glide.with(this)
            .load(imageUrl)
            .fitCenter()
            .into(detailsImageView)
    }

    private fun sendFeedback(featureId: String?) {
        sendImageView.setOnClickListener {
            notificationDetailsViewModel.sendFeedBack(featureId, feedbackEditText.text.toString())
            progressBar.visibility = View.VISIBLE
            sendImageView.visibility = View.GONE
            feedbackEditText.isEnabled = false
        }
    }

    private fun showMessage(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    private fun goBack() {
        backImageView.setOnClickListener {
            onBackPressed()
        }
    }
}
