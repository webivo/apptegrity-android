package io.apptegrity.ui.notificationdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import io.apptegrity.business.domainmodel.NotificationMapper
import io.apptegrity.business.usecase.NotificationUseCase
import io.apptegrity.business.utils.SingleLiveEvent
import io.apptegrity.ui.notifications.model.NotificationModel
import io.apptegrity.ui.notifications.model.FeedbackModel
import io.apptegrity.ui.utils.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NotificationDetailsViewModel(private val notificationUseCase: NotificationUseCase) : ViewModel() {

    private val _onFeatureDetailsEvent = SingleLiveEvent<NotificationModel>()
    private val _onSendFeedBackEvent = SingleLiveEvent<Event>()
    private val compositeDisposable = CompositeDisposable()

    val onNotificationDetailsEvent: LiveData<NotificationModel>
        get() = _onFeatureDetailsEvent

    val onSendFeedBackEvent: LiveData<Event>
        get() = _onSendFeedBackEvent

    fun showFeatureDetails(notificationModel: NotificationModel?) {
        notificationModel?.let {
            _onFeatureDetailsEvent.value = notificationModel
        }
    }

    fun sendOnFeatureOpened(featureId: String?) {
        if (featureId != null) {
            compositeDisposable.add(
                notificationUseCase.sendOnFeatureOpened(featureId)
                    .retry(3)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::onFeatureOpenedSubmitSuccess, this::onFeatureOpenedSubmitError)
            )
        }
    }

    fun sendFeedBack(featureId: String?, content: String?) {
        if (!content.isNullOrEmpty() && featureId != null) {
            val feedback = FeedbackModel(content)
            compositeDisposable.add(
                notificationUseCase.sendFeedback(featureId, NotificationMapper.toFeedBack(feedback))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::onFeedBackComplete, this::onFeedBackError)
            )
        } else {
            _onSendFeedBackEvent.postValue(Event.ObjectNull)
        }
    }

    private fun onFeatureOpenedSubmitSuccess() {

    }

    private fun onFeatureOpenedSubmitError(error: Throwable) {

    }

    private fun onFeedBackComplete() {
        _onSendFeedBackEvent.postValue(Event.NetworkSuccess(0))
    }

    private fun onFeedBackError(error: Throwable) {
        _onSendFeedBackEvent.postValue(Event.NetworkError(error.localizedMessage))
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}
