package io.apptegrity.ui.notifications

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils.loadLayoutAnimation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import io.apptegrity.Apptegrity
import io.apptegrity.R
import io.apptegrity.business.di.AppContainer
import io.apptegrity.business.di.FeatureContainer
import io.apptegrity.ui.notificationdetails.NotificationDetailsActivity
import io.apptegrity.ui.notifications.model.NotificationModel
import io.apptegrity.ui.utils.Errors
import io.apptegrity.ui.utils.Event
import io.apptegrity.ui.utils.FEATURE_DETAILS
import kotlinx.android.synthetic.main.activity_features.allFeaturesList
import kotlinx.android.synthetic.main.activity_features.progressBar
import kotlinx.android.synthetic.main.activity_features.allFeaturesErrorLayout
import kotlinx.android.synthetic.main.activity_features.swipeRefreshLayout
import kotlinx.android.synthetic.main.activity_features.getFeaturesAgainButton

class NotificationsActivity : AppCompatActivity() {

    private lateinit var notificationsViewModel: NotificationsViewModel
    private var appContainer: AppContainer? = null
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: NotificationsAdapter

    private val onFeaturesObserver = Observer<List<NotificationModel>> { allFeatures ->
        notificationsViewModel.sendOnAllFeaturesOpened()
        setAdapter(allFeatures)
    }

    private val onFeaturesErrorObserver = Observer<Errors> {
        setFeaturesErrorLayout()
    }

    private val onFeatureDetailsObserver = Observer<NotificationModel> { feature ->
        val intent = Intent(this, NotificationDetailsActivity::class.java)
        intent.putExtra(FEATURE_DETAILS, feature)
        startActivity(intent)
    }

    private val onFeedBackPostObserver = Observer<Event> {
        when (it) {
            is Event.NetworkSuccess -> {
                val viewHolder =
                    allFeaturesList.findViewHolderForAdapterPosition(it.position) as NotificationsAdapter.ViewHolder
                viewHolder.onFeedBackSubmitSuccess()
                showMessage(getString(R.string.post_feature_feedback_success_text))

            }
            is Event.NetworkError -> showMessage(getString(R.string.post_feature_feedback_error_content_text))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_features)
        appContainer = Apptegrity.appContainer
        appContainer?.let {
            it.featureContainer = FeatureContainer(it.notificationsApi)
            val featureContainer = it.featureContainer
            if (featureContainer != null) {
                notificationsViewModel = featureContainer.featureViewModel
                registerObservers()
                progressBar.visibility = View.VISIBLE
                notificationsViewModel.getAllFeatures()
                adapter = NotificationsAdapter(
                    { position, featureId, content ->
                        notificationsViewModel.sendFeedback(position, featureId, content)
                    },
                    { position, featureModel ->
                        notificationsViewModel.openDetails(featureModel)
                    }
                )
                getAllFeaturesAgain()
                refreshData()
            }
        }
    }

    override fun onDestroy() {
        appContainer?.featureContainer = null
        super.onDestroy()
    }

    private fun registerObservers() {
        notificationsViewModel.onFeaturesEvent.observe(this, onFeaturesObserver)
        notificationsViewModel.onFeaturesError.observe(this, onFeaturesErrorObserver)
        notificationsViewModel.onNotificationDetailsEvent.observe(this, onFeatureDetailsObserver)
        notificationsViewModel.onFeedBackPostEvent.observe(this, onFeedBackPostObserver)
    }

    private fun setAdapter(allNotifications: List<NotificationModel>) {
        progressBar.visibility = View.GONE
        runLayoutAnimation()
        setSwipeRefreshLoading()
        allFeaturesErrorLayout.visibility = View.GONE
        swipeRefreshLayout.visibility = View.VISIBLE
        adapter.setAllFeatures(allNotifications)
        linearLayoutManager = LinearLayoutManager(this)
        allFeaturesList.layoutManager = linearLayoutManager
        allFeaturesList.adapter = adapter
    }

    private fun setFeaturesErrorLayout() {
        progressBar.visibility = View.GONE
        setSwipeRefreshLoading()
        swipeRefreshLayout.visibility = View.GONE
        allFeaturesErrorLayout.visibility = View.VISIBLE
    }

    private fun getAllFeaturesAgain() {
        getFeaturesAgainButton.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            notificationsViewModel.getAllFeatures()
        }
    }

    private fun refreshData() {
        swipeRefreshLayout.setOnRefreshListener {
            adapter.clear()
            notificationsViewModel.getAllFeatures()
        }
    }

    private fun setSwipeRefreshLoading() {
        if (swipeRefreshLayout.isRefreshing) {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun runLayoutAnimation() {
        val context = allFeaturesList.context
        val controller = loadLayoutAnimation(context, R.anim.layout_animation_fall_down)

        allFeaturesList.layoutAnimation = controller
        allFeaturesList.adapter?.notifyDataSetChanged()
        allFeaturesList.scheduleLayoutAnimation()
    }

    private fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
