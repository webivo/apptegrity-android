package io.apptegrity.ui.notifications

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.apptegrity.R
import io.apptegrity.ui.notifications.model.NotificationModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_features.itemLayout
import kotlinx.android.synthetic.main.item_features.featureImageView
import kotlinx.android.synthetic.main.item_features.contentTextView
import kotlinx.android.synthetic.main.item_features.statusTextView
import kotlinx.android.synthetic.main.item_features.dateTextView
import kotlinx.android.synthetic.main.item_features.feedbackEditText
import kotlinx.android.synthetic.main.item_features.progressBar
import kotlinx.android.synthetic.main.item_features.sendImageView
import kotlinx.android.synthetic.main.item_features.titleTextView

class NotificationsAdapter(
    private val onSubmitFeedbackClick: (position: Int, featureId: String, content: String) -> Unit,
    private val onItemClick: (position: Int, notificationModel: NotificationModel) -> Unit
) :
    RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {

    private lateinit var allNotifications: List<NotificationModel>

    fun setAllFeatures(allNotifications: List<NotificationModel>) {
        this.allNotifications = allNotifications
    }

    fun clear() {
        allNotifications = emptyList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_features, parent, false)
        return ViewHolder(
            view,
            onSubmitFeedbackClick,
            onItemClick
        )
    }

    override fun getItemCount(): Int {
        return allNotifications.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val feature = allNotifications[position]
        holder.bindData(feature)
    }

    class ViewHolder(
        itemView: View,
        val onSubmitFeedbackClick: (position: Int, featureId: String, content: String) -> Unit,
        val onItemClick: (position: Int, notificationModel: NotificationModel) -> Unit
    ) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View?
            get() = itemView

        fun bindData(notification: NotificationModel) {
            ViewCompat.setTransitionName(itemLayout, notification.title)

            notification.imageUrl?.let { url ->
                featureImageView.visibility = View.VISIBLE
                Glide.with(itemView)
                    .load(url)
                    .into(featureImageView)
            }
            titleTextView.text = notification.title
            contentTextView.text = HtmlCompat.fromHtml(notification.content, FROM_HTML_MODE_LEGACY)
            setStatusDrawable(notification.status)
            statusTextView.text = notification.status
            dateTextView.text = notification.date
            onFeedbackAddTextChangedListener()
            onSubmitFeedbackClickListener(notification.id)
            onItemClickListener(notificationModel = notification)
        }

        fun onFeedBackSubmitSuccess() {
            progressBar.visibility = View.GONE
            feedbackEditText.text.clear()
            feedbackEditText.isEnabled = true
        }

        private fun onFeedbackAddTextChangedListener() {
            feedbackEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (!s.isNullOrEmpty()) {
                        sendImageView.visibility = View.VISIBLE
                    }

                    if (s.isNullOrEmpty()) {
                        sendImageView.visibility = View.GONE
                    }
                }

            })
        }

        private fun onSubmitFeedbackClickListener(featureId: String) {
            sendImageView.setOnClickListener {
                val content = feedbackEditText.text.toString()
                progressBar.visibility = View.VISIBLE
                sendImageView.visibility = View.GONE
                feedbackEditText.isEnabled = false
                onSubmitFeedbackClick(adapterPosition, featureId, content)
            }
        }

        private fun onItemClickListener(notificationModel: NotificationModel) {
            itemLayout.setOnClickListener {
                onItemClick(adapterPosition, notificationModel)
            }
        }

        private fun setStatusDrawable(status: String) {
            when (status) {
                "Announcement" -> statusTextView.background =
                    itemView.context.getDrawable(R.drawable.orange_status_drawable)
                "Coming Soon" -> statusTextView.background =
                    itemView.context.getDrawable(R.drawable.green_status_drawable)
                "Fix" -> statusTextView.background =
                    itemView.context.getDrawable(R.drawable.blue_status_drawable)
                "Improvement" -> statusTextView.background =
                    itemView.context.getDrawable(R.drawable.violet_status_drawable)
                "New" -> statusTextView.background =
                    itemView.context.getDrawable(R.drawable.red_status_drawable)
            }
        }
    }
}