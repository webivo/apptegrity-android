package io.apptegrity.ui.notifications

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.apptegrity.business.domainmodel.NotificationMapper
import io.apptegrity.business.usecase.NotificationUseCase
import io.apptegrity.business.utils.SingleLiveEvent
import io.apptegrity.ui.notifications.model.NotificationModel
import io.apptegrity.ui.notifications.model.FeedbackModel
import io.apptegrity.ui.utils.Errors
import io.apptegrity.ui.utils.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NotificationsViewModel(private val notificationUseCase: NotificationUseCase) : ViewModel() {

    private val disposable = CompositeDisposable()

    private val _onFeaturesEvent = MutableLiveData<List<NotificationModel>>()
    private val _onFeaturesErrorEvent = MutableLiveData<Errors>()
    private val _onFeaturesDetailsEvent = SingleLiveEvent<NotificationModel>()
    private val _onFeedBackPostEvent = SingleLiveEvent<Event>()

    val onFeaturesEvent: LiveData<List<NotificationModel>>
        get() = _onFeaturesEvent

    val onFeaturesError: LiveData<Errors>
        get() = _onFeaturesErrorEvent

    val onNotificationDetailsEvent: LiveData<NotificationModel>
        get() = _onFeaturesDetailsEvent

    val onFeedBackPostEvent: LiveData<Event>
        get() = _onFeedBackPostEvent

    fun getAllFeatures() {
        disposable.add(
            notificationUseCase.getAllFeatures()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::onAllFeaturesSuccess, this::onAllFeaturesError)
        )
    }

    fun sendFeedback(position: Int, featureId: String, content: String) {
        val feedback = FeedbackModel(content)
        disposable.add(
            notificationUseCase.sendFeedback(featureId, NotificationMapper.toFeedBack(feedback))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    { onFeedBackComplete(position) },
                    { throwable -> onFeedbackError(throwable) })
        )
    }

    fun sendOnAllFeaturesOpened() {
        disposable.add(
            notificationUseCase.sendOnAllFeaturesOpened()
                .retry(3)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    this::onAllFeaturesOpenedSubmitSuccess,
                    this::onAllFeaturesOpenedSubmitError
                )
        )
    }

    fun openDetails(notificationModel: NotificationModel) {
        _onFeaturesDetailsEvent.value = notificationModel
    }

    private fun onAllFeaturesSuccess(allNotifications: List<NotificationModel>) {
        _onFeaturesEvent.value = allNotifications
    }

    private fun onAllFeaturesError(error: Throwable) {
        _onFeaturesErrorEvent.postValue(Errors.GetAllFeatures(error))
    }

    private fun onFeedBackComplete(position: Int) {
        _onFeedBackPostEvent.postValue(Event.NetworkSuccess(position))
    }

    private fun onFeedbackError(error: Throwable) {
        _onFeedBackPostEvent.postValue(Event.NetworkError(error.localizedMessage))
    }

    private fun onAllFeaturesOpenedSubmitSuccess() {

    }

    private fun onAllFeaturesOpenedSubmitError(error: Throwable) {

    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}
