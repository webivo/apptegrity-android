package io.apptegrity.ui.notifications.model

data class FeedbackModel(
    val text: String
)