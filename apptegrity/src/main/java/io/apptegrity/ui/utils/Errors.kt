package io.apptegrity.ui.utils

sealed class Errors {
    data class GetAllFeatures(val error: Throwable) : Errors()
    data class PostFeatureFeedback(val error: Throwable): Errors()
}