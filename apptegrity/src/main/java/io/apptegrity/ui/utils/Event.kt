package io.apptegrity.ui.utils

sealed class Event {
    data class NetworkSuccess(val position: Int) : Event()
    data class NetworkError(val error: String) : Event()
    object ObjectNull : Event()
}
